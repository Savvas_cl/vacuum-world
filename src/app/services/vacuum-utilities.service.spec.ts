import { TestBed } from '@angular/core/testing';

import { VacuumUtilitiesService } from './vacuum-utilities.service';

describe('VacuumUtilitiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('VacuumUtilitesService should be created', () => {
    const service: VacuumUtilitiesService = TestBed.get(VacuumUtilitiesService);
    expect(service).toBeTruthy();
  });

  it('should have an executeRobotMove() function', () => {
    const service: VacuumUtilitiesService = TestBed.get(VacuumUtilitiesService);
    expect(service.executeRobotMove).toBeDefined();
  });

  it('executeRobotMove() should change robot coordinates', () => {
    const service: VacuumUtilitiesService = TestBed.get(VacuumUtilitiesService);
    const robotCoordinates = {x: 1, y: 2};

    service.executeRobotMove(robotCoordinates);

    expect(robotCoordinates).not.toEqual({x: 1, y: 2});
  });

  it('dispatch table command moveNorth should remove one from x coordinate', () => {
    const service: VacuumUtilitiesService = TestBed.get(VacuumUtilitiesService);
    const robotCoordinates = {x: 1, y: 2};

    service.robotCommands.moveNorth(robotCoordinates);

    expect(robotCoordinates).toEqual({x: 0, y: 2});
  });

  it('dispatch table command moveNorth should do nothing if x coordinate equals 0', () => {
    const service: VacuumUtilitiesService = TestBed.get(VacuumUtilitiesService);
    const robotCoordinates = {x: 0, y: 2};

    service.robotCommands.moveNorth(robotCoordinates);

    expect(robotCoordinates).toEqual({x: 0, y: 2});
  });

  it('dispatch table command moveSouth should add one to x coordinate', () => {
    const service: VacuumUtilitiesService = TestBed.get(VacuumUtilitiesService);
    const robotCoordinates = {x: 1, y: 2};

    service.robotCommands.moveSouth(robotCoordinates);

    expect(robotCoordinates).toEqual({x: 2, y: 2});
  });

  it('dispatch table command moveSouth should do nothing if x coordinate equals 9', () => {
    const service: VacuumUtilitiesService = TestBed.get(VacuumUtilitiesService);
    const robotCoordinates = {x: 9, y: 2};

    service.robotCommands.moveSouth(robotCoordinates);

    expect(robotCoordinates).toEqual({x: 9, y: 2});
  });

  it('dispatch table command moveEast should add one to y coordinate', () => {
    const service: VacuumUtilitiesService = TestBed.get(VacuumUtilitiesService);
    const robotCoordinates = {x: 1, y: 2};

    service.robotCommands.moveEast(robotCoordinates);

    expect(robotCoordinates).toEqual({x: 1, y: 3});
  });

  it('dispatch table command moveEast should do nothing if y coordinate equals 9', () => {
    const service: VacuumUtilitiesService = TestBed.get(VacuumUtilitiesService);
    const robotCoordinates = {x: 1, y: 9};

    service.robotCommands.moveEast(robotCoordinates);

    expect(robotCoordinates).toEqual({x: 1, y: 9});
  });

  it('dispatch table command moveWest should remove one from y coordinate', () => {
    const service: VacuumUtilitiesService = TestBed.get(VacuumUtilitiesService);
    const robotCoordinates = {x: 1, y: 2};

    service.robotCommands.moveWest(robotCoordinates);

    expect(robotCoordinates).toEqual({x: 1, y: 1});
  });

  it('dispatch table command moveWest should do nothing if y coordinate equals 0', () => {
    const service: VacuumUtilitiesService = TestBed.get(VacuumUtilitiesService);
    const robotCoordinates = {x: 1, y: 0};

    service.robotCommands.moveWest(robotCoordinates);

    expect(robotCoordinates).toEqual({x: 1, y: 0});
  });
});
