import { Injectable } from '@angular/core';
import { Coordinates } from '../models/coordinates';

@Injectable({
  providedIn: 'root'
})
export class VacuumUtilitiesService {
  // Dispatch table contains the moves allowed by the robot
  public robotCommands = {
    moveNorth(robotCoordinates: Coordinates) {
      if (robotCoordinates.x > 0) {
        robotCoordinates.x--;
      }
    },
    moveSouth(robotCoordinates: Coordinates) {
      if (robotCoordinates.x < 9) {
        robotCoordinates.x++;
      }
    },
    moveEast(robotCoordinates: Coordinates) {
      if (robotCoordinates.y < 9) {
        robotCoordinates.y++;
      }
    },
    moveWest(robotCoordinates: Coordinates) {
      if (robotCoordinates.y > 0) {
        robotCoordinates.y--;
      }
    }
  };

  public robotCommandsIndex = {
    0: 'moveNorth',
    1: 'moveSouth',
    2: 'moveEast',
    3: 'moveWest'
  };

  constructor() { }

  // Return a random number from 0 to max number, non-inclusive
  getRandomInt(max: number): number {
    return Math.floor(Math.random() * Math.floor(max));
  }

  // Execute a random robot move using the dispatch table above.
  executeRobotMove(robotCoordinates: Coordinates) {
    const command = this.robotCommandsIndex[this.getRandomInt(4)];
    this.robotCommands[command](robotCoordinates);
  }

}
