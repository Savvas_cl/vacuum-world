import { async, ComponentFixture, TestBed, fakeAsync, inject, tick, discardPeriodicTasks } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { VacuumUtilitiesService } from '../../services/vacuum-utilities.service';
import { Coordinates } from '../../models/coordinates';
import { FloorTileComponent } from './floor-tile.component';

@Component({selector: 'app-tile', template: ''})
class TileComponent {
  @Input() robotCoordinates: Coordinates;
  @Input() tileCoordinates: Coordinates;
}

describe('FloorTileComponent', () => {
  let component: FloorTileComponent;
  let fixture: ComponentFixture<FloorTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FloorTileComponent, TileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloorTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create FloorTileComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should have resetFloor() function defined', () => {
    expect(component.resetFloor).toBeDefined();
  });

  it('resetFloor() should set cleanTiles variable to 0', () => {
    component.cleanTiles = 10;

    component.resetFloor();

    expect(component.cleanTiles).toEqual(0);
  });

  it('should have addCleanTile() function defined', () => {
    expect(component.addCleanTile).toBeDefined();
  });

  it('addCleanTile() should add 1 to cleanTiles variable', () => {
    component.cleanTiles = 10;

    component.addCleanTile();

    expect(component.cleanTiles).toEqual(11);
  });

  it('addCleanTile() should emit floorIsClean event when cleanTiles equals 100', () => {
    component.cleanTiles = 99;
    spyOn(component.floorIsClean, 'emit');

    component.addCleanTile();

    expect(component.floorIsClean.emit).toHaveBeenCalled();
  });

  it('should have a moveRobot() function defined', () => {
    expect(component.moveRobot).toBeDefined();
  });

  it('moveRobot() should call clearInterval when cleanTiles equals 100', fakeAsync(() => {
    component.cleanTiles = 100;
    spyOn(window, 'clearInterval');
    fixture.detectChanges();

    component.moveRobot();
    tick(1000);
    discardPeriodicTasks();

    fixture.whenStable().then(() => {
      expect(clearInterval).toHaveBeenCalled();
    });
  }));

  it('moveRobot() should call executeRobotMove when cleanTiles equals les than 100', fakeAsync(inject([VacuumUtilitiesService], s => {
    const utilitiesService = s;
    component.cleanTiles = 78;
    spyOn(utilitiesService, 'executeRobotMove');
    fixture.detectChanges();

    component.moveRobot();
    tick(1000);
    discardPeriodicTasks();

    fixture.whenStable().then(() => {
      expect(utilitiesService.executeRobotMove).toHaveBeenCalled();
    });
  })));
});
