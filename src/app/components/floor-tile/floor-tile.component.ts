import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { VacuumUtilitiesService } from '../../services/vacuum-utilities.service';
import { Coordinates } from '../../models/coordinates';

@Component({
  selector: 'app-floor-tile',
  templateUrl: './floor-tile.component.html',
  styleUrls: ['./floor-tile.component.scss']
})
export class FloorTileComponent implements OnInit, OnDestroy {
  @Output() floorIsClean = new EventEmitter<boolean>();
  public floorTileRow: number[];
  public floorTile: Array<Array<number>>;
  public robot: Coordinates;
  private intervalId: number;
  public cleanTiles = 0;

  constructor(private utilities: VacuumUtilitiesService) { }

  ngOnInit() {
    this.initializeWorld();
  }

  initializeWorld() {
    this.initializeFloor(10);
    this.robot = new Coordinates(this.utilities.getRandomInt(10), this.utilities.getRandomInt(10));
    this.moveRobot();
  }

  resetFloor() {
    this.cleanTiles = 0;
    window.clearInterval(this.intervalId);
    this.initializeWorld();
  }

  ngOnDestroy() {
    if (this.intervalId) {
      window.clearInterval(this.intervalId);
    }
  }

  initializeFloor(dimension: number) {
    this.floorTile = new Array(dimension);
    for (let i = 0; i < this.floorTile.length; i++) {
      this.floorTile[i] = new Array(dimension);
    }
  }

  addCleanTile() {
    this.cleanTiles++;
    if (this.cleanTiles === 100) {
      this.floorIsClean.emit(true);
    }
  }

  moveRobot() {
    this.intervalId = window.setInterval( () => {
      if (this.cleanTiles === 100) {
        window.clearInterval(this.intervalId);
      } else {
        this.utilities.executeRobotMove(this.robot);
      }
    }, 500);
  }

}
