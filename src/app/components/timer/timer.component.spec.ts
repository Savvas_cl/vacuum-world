import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TimerComponent } from './timer.component';

describe('TimerComponent', () => {
  let component: TimerComponent;
  let fixture: ComponentFixture<TimerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a TimerComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should have a restartTimer() function defined', () => {
    expect(component.restartTimer).toBeDefined();
  });

  it('restartTimer() should set counterValue to 0', () => {
    component.counterValue = 123;

    component.restartTimer();

    expect(component.counterValue).toEqual(0);
  });

  it('component should open the modal if stopTimer variable is true', inject([NgbModal], (s) => {
    const modalService = s;
    component.stopTimer = true;
    fixture.detectChanges();
    spyOn(modalService, 'open');
    component.ngOnChanges();

    expect(modalService.open).toHaveBeenCalled();
  }));
});
