import { Component, Input, OnInit, OnChanges, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit, OnDestroy, OnChanges {
  public counterValue: number;
  public seconds: number;
  public minutes: number;
  public hours: number;
  private intervalTimerId: number;
  @Input() stopTimer: boolean;
  @ViewChild('content', { static: false }) modalContent: ElementRef;

  constructor(private modalService: NgbModal) {
    this.counterValue = 0;
  }

  ngOnInit() {
    this.calculateTimeUnits(this.counterValue);
    this. intervalTimerId = window.setInterval( () => {
      this.counterValue++;
      this.calculateTimeUnits(this.counterValue);
    }, 1000);
  }

  ngOnChanges() {
    if (this.stopTimer) {
      this.modalService.open(this.modalContent);
      window.clearInterval(this.intervalTimerId);
    }
  }

  restartTimer() {
    this.counterValue = 0;
    this.calculateTimeUnits(this.counterValue);
    this.stopTimer = false;
    window.clearInterval(this.intervalTimerId);

    this. intervalTimerId = window.setInterval( () => {
      this.counterValue++;
      this.calculateTimeUnits(this.counterValue);
    }, 1000);
  }

  ngOnDestroy() {
    if (this.intervalTimerId) {
      window.clearInterval(this.intervalTimerId);
    }
  }

  calculateTimeUnits(seconds) {
    this.seconds = seconds % 60;
    this.minutes = Math.floor(seconds / 60) % 60;
    this.hours = Math.floor(seconds / 3600);
  }
}
