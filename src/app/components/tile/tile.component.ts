import { Component, OnInit, AfterViewChecked, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { VacuumUtilitiesService } from '../../services/vacuum-utilities.service';
import { Coordinates } from '../../models/coordinates';

@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent implements OnInit, OnDestroy, AfterViewChecked {
  @Input() robotCoordinates: Coordinates;
  @Input() tileCoordinates: Coordinates;
  @Output() isClean = new EventEmitter<boolean>();
  public dirtiness: number;
  public timeoutId: number;

  constructor(private utilities: VacuumUtilitiesService) { }

  ngOnInit() {
    this.dirtiness = this.utilities.getRandomInt(2);
    if (this.dirtiness === 0) {
      this.isClean.emit(true);
    }
  }

  ngAfterViewChecked() {
    if (this.isRobotHere()) {
      this.clean();
    }
  }

  ngOnDestroy() {
    window.clearTimeout(this.timeoutId);
  }

  isRobotHere(): boolean {
    return ( this.tileCoordinates.x === this.robotCoordinates.x)
            && (this.tileCoordinates.y === this.robotCoordinates.y);
  }

  clean() {
    // Needs to be in setTimeout otherwise error occurs
    // (view changed after expression was checked)
    window.setTimeout( () => {
      if (this.dirtiness) {
        this.dirtiness--;
        if (this.dirtiness === 0) {
          this.isClean.emit(true);
        }
      }
    }, 0);
  }
}
