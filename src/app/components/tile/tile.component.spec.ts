import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { TileComponent } from './tile.component';

describe('TileComponent', () => {
  let component: TileComponent;
  let fixture: ComponentFixture<TileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileComponent);
    component = fixture.componentInstance;
    component.robotCoordinates = { x: 1, y: 2};
    component.tileCoordinates = {x: 1, y: 3};
    fixture.detectChanges();
  });

  it('should create TileComponent', () => {
    expect(component).toBeTruthy();
  });

  it('it should have a isRobotHere() function defined', () => {
    expect(component.isRobotHere).toBeDefined();
  });

  it('isRobotHere() should return TRUE when tile and robot coordinates are the same', () => {
    component.robotCoordinates = { x: 1, y: 2};
    component.tileCoordinates = { x: 1, y: 2};
    fixture.detectChanges();

    component.isRobotHere();

    expect(component.isRobotHere()).toBeTruthy();
  });

  it('isRobotHere() should return FALSE when tile and robot coordinates are not the same', () => {
    component.robotCoordinates = { x: 1, y: 3};
    component.tileCoordinates = { x: 4, y: 2};
    fixture.detectChanges();

    component.isRobotHere();

    expect(component.isRobotHere()).toBeFalsy();
  });

  it('it should have a clean() function defined', () => {
    expect(component.clean).toBeDefined();
  });

  it('clean should set dirtiness to zero if tile is dirty', fakeAsync(() => {
    component.dirtiness = 1;

    component.clean();
    tick(100);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.dirtiness).toEqual(0);
    });
  }));

  it('clean should set do nothing if tile is clean', fakeAsync(() => {
    component.dirtiness = 0;

    component.clean();
    tick(100);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.dirtiness).toEqual(0);
    });
  }));

  it('clean should not emit isClean event if tile is not clean', fakeAsync(() => {
    component.dirtiness = 2;
    spyOn(component.isClean, 'emit');

    component.clean();
    tick(100);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.isClean.emit).not.toHaveBeenCalled();
    });
  }));
});
