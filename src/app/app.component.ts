import { Component, ViewChild } from '@angular/core';
import { FloorTileComponent } from './components/floor-tile/floor-tile.component';
import { TimerComponent } from './components/timer/timer.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'vacuum';
  public showCleaningInfo = false;
  @ViewChild('floor', { static: false }) floorView: FloorTileComponent;
  @ViewChild('timer', { static: false }) timerView: TimerComponent;

  cleaningComplete() {
    this.showCleaningInfo = true;
  }

  resetWorld() {
    this.timerView.restartTimer();
    this.showCleaningInfo = false;
    this.floorView.resetFloor();
  }
}
