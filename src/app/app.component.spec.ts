import { TestBed, async } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { AppComponent } from './app.component';
import { FloorTileComponent } from './components/floor-tile/floor-tile.component';
import { TimerComponent } from './components/timer/timer.component';

@Component({selector: 'app-tile', template: ''})
class TileComponent {
  @Input() robotCoordinates: Coordinates;
  @Input() tileCoordinates: Coordinates;
}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        FloorTileComponent,
        TimerComponent,
        TileComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'vacuum'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('vacuum');
  });

  it('should have a cleaningComplete() function defined', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.cleaningComplete).toBeDefined();
  });

  it('cleaningComplete() should set showCleaningInfo variable to true', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    app.showCleaningInfo = false;
    app.cleaningComplete();

    expect(app.showCleaningInfo).toBeTruthy();
  });

  it('should have a resetWorld() function defined', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.resetWorld).toBeDefined();
  });

  it('resetWorld() should set showCleaningInfo variable to false', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    fixture.detectChanges();

    app.showCleaningInfo = true;
    spyOn(app.timerView, 'restartTimer');
    spyOn(app.floorView, 'resetFloor');
    app.resetWorld();

    expect(app.showCleaningInfp).toBeFalsy();
  });

});
