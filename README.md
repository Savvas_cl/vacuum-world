# Vacuum

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.

A simple version of the Vacuum World problem.

Below you can find the commands to run the project. Note that unit tests have been added. Along with angular I also used bootstrap and ng-bootstrap packages. Thank you for your time.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

